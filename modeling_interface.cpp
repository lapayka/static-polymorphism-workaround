#include <boost/test/unit_test.hpp>

#include <type_traits>
#include <sstream>

template<typename T>
concept InterfaceMathcable = requires(T a, std::ostream & stream)
{
    {a.PolyMethod(stream)} -> std::same_as<void>;
} && ((&T::PolyMethod) != (&T::parent::PolyMethod));

class Interface
{
private:
    void  (*m_pfnRun_T)(void* const, std::ostream &stream);

    template<InterfaceMathcable T>
    static void PolyAdapter(void* const pObj, std::ostream& stream) 
    { 
        static_cast<T* const>(pObj)->PolyMethod(stream); 
    }
protected:
    template<InterfaceMathcable T>
    void  Init()
    {
        static_assert(InterfaceMathcable<T>);
        m_pfnRun_T = &PolyAdapter<T>;
    }
public:
    using parent = typename Interface;

    Interface() : m_pfnRun_T(0) {}
    virtual ~Interface() {}
    void PolyMethod(std::ostream& stream)
    {
        assert(m_pfnRun_T);
        return (*m_pfnRun_T)(this, stream);
    }
};

class InterfaceImpl1 : public Interface
{
public:
    InterfaceImpl1()
    {
        Init<std::remove_reference_t<decltype(*this)>>();
    }


    void PolyMethod(std::ostream& stream)
    {
        stream << "1";
    }
};

class InterfaceImpl2 : public Interface
{
public:
    InterfaceImpl2()
    {
        Init<std::remove_reference_t<decltype(*this)>>();
    }


    void PolyMethod(std::ostream& stream)
    {
        stream << "2";
    }
};

class InterfaceImpl3 : public Interface
{
public:
    InterfaceImpl3()
    {
        // won't compile because there is no polymethod
        // Init<std::remove_reference_t<decltype(*this)>>();
    }
};

BOOST_AUTO_TEST_CASE(simple_test)
{
    InterfaceImpl1 i1;
    InterfaceImpl2 i2;
    static_assert(!InterfaceMathcable<InterfaceImpl3>);

    std::stringstream ss;

    i1.PolyMethod(ss);
    i2.PolyMethod(ss);

    BOOST_REQUIRE(ss.str() == "12");
}

BOOST_AUTO_TEST_CASE(base_pointer_test)
{
    std::shared_ptr<Interface> base_ptr = std::make_shared<InterfaceImpl1>();

    std::stringstream ss;

    base_ptr->PolyMethod(ss);

    base_ptr = std::make_shared<InterfaceImpl2>();
    base_ptr->PolyMethod(ss);

    BOOST_REQUIRE(ss.str() == "12");
}

BOOST_AUTO_TEST_CASE(base_ptr_container)
{
    std::vector<std::shared_ptr<Interface>> base_ptr_vector =
    {
            std::make_shared<InterfaceImpl1>(),
            std::make_shared<InterfaceImpl2>(),
            std::make_shared<InterfaceImpl1>(),
            std::make_shared<InterfaceImpl1>(),
            std::make_shared<InterfaceImpl2>()
    };

    std::stringstream ss;

    for (const auto& base : base_ptr_vector)
    {
        base->PolyMethod(ss);
    }

    BOOST_REQUIRE(ss.str() == "12112");
}