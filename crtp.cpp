#define BOOST_TEST_MODULE static-poly-tests

#include <boost/test/unit_test.hpp>

#include <type_traits>

#include <sstream>

template <typename T>  
concept with_poly_method = requires(T a, std::ostream & stream)
{
	{ a.PolyMethod(stream) } -> std::same_as<void>;
};

template<typename T>
class Base
{
public:
	void PolyMethodAdapter(std::ostream& stream) 
	{
		//some logic with virtual methods
		static_cast<T&>(*this).PolyMethod(stream); 
	}
};

template<with_poly_method T> // won't work as base class because there is no information about PolyMethod is class when inherit requirements being checked
class BaseWithConcept
{
public:
	void PolyMethodAdapter(std::ostream& stream) { static_cast<T&>(*this).PolyMethod(stream); }
};

class Derived1 : public Base<Derived1>
{
public:
	void PolyMethod(std::ostream &stream)
	{
		stream << "Derived1";
	}
};

class Derived2 : public Base<Derived2>
{
public:
	void PolyMethod(std::ostream& stream)
	{
		stream << "Derived2";
	}
};


BOOST_AUTO_TEST_CASE(CRTP_ut)
{
	Derived1 der1;
	Derived2 der2;

	static_assert(with_poly_method<Derived1>);
	static_assert(with_poly_method<Derived2>);

	std::stringstream ss;
	der1.PolyMethodAdapter(ss);
	der2.PolyMethodAdapter(ss);

	BOOST_REQUIRE(ss.str() == "Derived1Derived2");
}