#include <boost/test/unit_test.hpp>

#include <type_traits>
#include <sstream>
#include <variant>


template<typename T>
concept StreamSerializable = 
	requires(const T &a, std::ostream & s)
{
	{ a.Save(s) } -> std::same_as<std::ostream&>;
}
&& requires(T& a, std::istream& s)
{
	{ a.Load(s) } -> std::same_as<std::istream&>;
};

template<typename T1>
constexpr bool CanBeBaseHelp()
{
	return StreamSerializable<T1>;
};

template<typename T1, typename T2, typename... Args>
constexpr bool CanBeBaseHelp()
{
	return StreamSerializable<T1> && StreamSerializable<T2> && CanBeBaseHelp<Args...>();
}

template<typename... Args>
concept CanBeBase = CanBeBaseHelp<Args...>();

template<CanBeBase... Args>
class BaseSerialized
{
	std::variant<Args...> m_data;

public:

	template<StreamSerializable _T>
	BaseSerialized(const _T& data)
		: m_data(data)
	{
	}
	

	std::ostream& Save(std::ostream& s) const {
		std::visit(
			[&s](auto&& concrete)
			{
				concrete.Save(s);
			}, m_data);
		return s;
	};
	std::istream& Load(std::istream& s) {
		assert(0);
		/* non-trivial logic there is no case to realize it*/
		return s;
	};
};



class IntSerializableClass
{
	int a = 1;
public:
	std::ostream &Save(std::ostream &s) const {
		return (s << a);
	};
	std::istream &Load(std::istream& s) {
		return (s >> a);
	};
};

class StringSerializableClass
{
	std::string str;
public:
	StringSerializableClass() 
		: str("abc") 
	{}

	std::ostream& Save(std::ostream& s) const {
		return (s << str);
	};
	std::istream& Load(std::istream& s) {
		return (s >> str);
	};
};

BOOST_AUTO_TEST_CASE(simpleSerialization)
{
	using baseSerializable = BaseSerialized<StringSerializableClass, IntSerializableClass>;

	std::vector<baseSerializable> vector_of_serializables =
	{
		StringSerializableClass(),
		IntSerializableClass(),
		IntSerializableClass()
	};

	std::stringstream ss;
	for (const auto& base : vector_of_serializables)
	{
		base.Save(ss);
	}

	BOOST_REQUIRE(ss.str() == "abc11");
}