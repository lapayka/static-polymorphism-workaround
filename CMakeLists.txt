cmake_minimum_required( VERSION 3.24 )

set( CMAKE_CXX_STANDARD 20 )

project( static-polymorphism-examples CXX )

find_package( Boost REQUIRED COMPONENTS unit_test_framework  )

add_executable( ${PROJECT_NAME} )

target_sources( ${PROJECT_NAME}
	PRIVATE
		crtp.cpp
		modeling_interface.cpp
 "static_poly.cpp")

target_link_libraries( ${PROJECT_NAME} 
	PRIVATE
		Boost::unit_test_framework
)
